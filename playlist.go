// Copyright © 2011-12 Qtrac Ltd.
//
// This program or package and any associated files are licensed under the
// Apache License, Version 2.0 (the "License"); you may not use these files
// except in compliance with the License. You can get a copy of the License
// at: http://www.apache.org/licenses/LICENSE-2.0.
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

type Song struct {
	Title    string
	Filename string
	Seconds  int
}

func main() {
	if len(os.Args) == 1 || (!strings.HasSuffix(os.Args[1], ".m3u") && !strings.HasSuffix(os.Args[1], ".pls")) {
		fmt.Printf("usage: %s <fileIn.[pls|m3u]> [<fileOut[.m3u|pls]>]\n", filepath.Base(os.Args[0]))
		os.Exit(1)
	}

	if rawBytes, err := ioutil.ReadFile(os.Args[1]); err != nil {
		log.Fatal(err)
	} else if strings.HasSuffix(os.Args[1], ".pls") {
		songs := readPlsPlaylist(string(rawBytes))
		writeM3uPlaylist(songs)
	} else if strings.HasSuffix(os.Args[1], ".m3u") {
		songs := readM3uPlaylist(string(rawBytes))
		writePlsPlaylist(songs)
	}
}

func readM3uPlaylist(data string) (songs []Song) {
	var song Song
	for _, line := range strings.Split(data, "\n") {
		line = strings.TrimSpace(line)
		if line == "" || strings.HasPrefix(line, "#EXTM3U") {
			continue
		}
		if strings.HasPrefix(line, "#EXTINF:") {
			song.Title, song.Seconds = parseExtinfLine(line)
		} else {
			song.Filename = strings.Map(mapPlatformDirSeparator, line)
		}
		if song.Filename != "" && song.Title != "" && song.Seconds != 0 {
			songs = append(songs, song)
			song = Song{}
		}
	}
	return songs
}

func parseExtinfLine(line string) (title string, seconds int) {
	if i := strings.IndexAny(line, "-0123456789"); i > -1 {
		const separator = ","
		line = line[i:]
		if j := strings.Index(line, separator); j > -1 {
			title = line[j+len(separator):]
			var err error
			if seconds, err = strconv.Atoi(line[:j]); err != nil {
				log.Printf("failed to read the duration for '%s': %v\n",
					title, err)
				seconds = -1
			}
		}
	}
	return title, seconds
}

func readPlsPlaylist(data string) (songs []Song) {
	var song Song
	for _, line := range strings.Split(data, "\n") {
		line = strings.TrimSpace(line)
		if line == "" || strings.HasPrefix(line, "[playlist]") ||
			strings.HasPrefix(line, "NumberOfEntries") || strings.HasPrefix(line, "Version") {
			continue
		}
		if strings.HasPrefix(line, "File") {
			song.Filename = parsePlsString(line)
		} else if strings.HasPrefix(line, "Title") {
			song.Title = parsePlsString(line)
		} else if strings.HasPrefix(line, "Length") {
			song.Seconds = parsePlsInteger(line)
		}
		if song.Filename != "" && song.Title != "" && song.Seconds != 0 {
			songs = append(songs, song)
			song = Song{}
		}
	}
	return songs
}

/* using for title and filename */
func parsePlsString(line string) (out string) {
	const separator = "="
	if i := strings.Index(line, separator); i > -1 {
		out = strings.Map(mapPlatformDirSeparator, line[i+len(separator):])
	}
	return out
}

/* using for seconds */
func parsePlsInteger(line string) (out int) {
	const separator = "="
	if i := strings.Index(line, separator); i > -1 {
		var err error
		if out, err = strconv.Atoi(line[i+len(separator):]); err != nil {
			log.Printf("failed to read the duration from \"%s\": %v\n",
				line, err)
			out = -1
		}
	}
	return out
}

func mapPlatformDirSeparator(char rune) rune {
	if char == '/' || char == '\\' {
		return filepath.Separator
	}
	return char
}

/* NOTE1: original program only displayed the result in the console.
My realization writes converted playlist into a new file, cool ya? */

/* NOTE2: according to the book += is not effictive enough (having O(n)),
and it's much faster to use var bytes.Buffer (O(1)?).
I will leave my original realization commented though
for the history (I don't have git installed here, soz). */

/* ORIGINAL
func writePlsPlaylist(songs []Song) {
	data := "[playlist]\n"
	for i, song := range songs {
		i++
		data += fmt.Sprintf("File%d=%s\n", i, song.Filename)
		data += fmt.Sprintf("Title%d=%s\n", i, song.Title)
		data += fmt.Sprintf("Length%d=%d\n", i, song.Seconds)
	}
	data += fmt.Sprintf("NumberOfEntries=%d\nVersion=2\n", len(songs))
	filename := filepath.Base(os.Args[1])
	filename = filename[:len(filename)-len(".m3u")]
	filename += ".pls"
	if err := ioutil.WriteFile(filename, []byte(data), 0); err != nil {
		log.Printf("%v", err)
	}
}

func writeM3uPlaylist(songs []Song) {
	data := "#EXTM3U\n"
	for _, song := range songs {
		data += fmt.Sprintf("#EXTINF:%d,%s\n", song.Seconds, song.Title)
		data += song.Filename
	}
	filename := filepath.Base(os.Args[1])
	filename = filename[:len(filename)-len(".pls")]
	filename += ".m3u"
	if err := ioutil.WriteFile(filename, []byte(data), 0); err != nil {
		log.Printf("%v", err)
	}
}
/ORIGINAL */

func setFileName(newExt string) (filename string) {
	oldExt := ".m3u"
	if newExt == ".m3u" {
		oldExt = ".pls"
	}
	if len(os.Args) < 3 {
		filename = filepath.Base(os.Args[1])[:len(filepath.Base(os.Args[1]))-len(oldExt)]
		filename += newExt // doing it once so not using bytes.Buffer
	} else { // custom user's filename
		filename = filepath.Base(os.Args[2])
		if !strings.HasSuffix(filename, newExt) {
			filename += newExt // doing it once so not using bytes.Buffer
		}
	}
	return filename
}

/* NOTE3: I used buf.WriteString(fmt.Print(...)) to write into the buf, but according to the book
at 5th chapter, using fmt.Fprint(&buf,...) is more comfortable, and I do agree */

func writePlsPlaylist(songs []Song) {
	var buf bytes.Buffer
	fmt.Fprint(&buf, "[playlist]\n")
	for i, song := range songs {
		i++
		fmt.Fprintf(&buf, "File%d=%s\n", i, song.Filename)
		fmt.Fprintf(&buf, "Title%d=%s\n", i, song.Title)
		fmt.Fprintf(&buf, "Length%d=%d\n", i, song.Seconds)
	}
	fmt.Fprintf(&buf, "NumberOfEntries=%d\nVersion=2\n", len(songs))
	filename := setFileName(".pls")
	if err := ioutil.WriteFile(filename, buf.Bytes(), 0); err != nil {
		log.Printf("%v", err)
	}
}

func writeM3uPlaylist(songs []Song) {
	var buf bytes.Buffer
	fmt.Fprint(&buf, "#EXTM3U\n")
	for _, song := range songs {
		fmt.Fprintf(&buf, "#EXTINF:%d,%s\n", song.Seconds, song.Title)
		fmt.Fprint(&buf, song.Filename)
		fmt.Fprint(&buf, "\n")
		/* will it be slower?
		   fmt.Fprint(&buf, song.Filename + "\n")
		*/
	}
	filename := setFileName(".m3u")
	if err := ioutil.WriteFile(filename, buf.Bytes(), 0); err != nil {
		log.Printf("%v", err)
	}
}
